package controlador;

import java.util.ArrayList;
import java.util.HashMap;

import logica.Algoritmo;
import logica.Grafo;

public class Controlador {
	private static Grafo grafo;

	public static ArrayList<Integer> generarConjuntoMinimo(HashMap<Integer,ArrayList<Integer>>verticeConVecinos,
			int cantidadVertices){
		 	

			Grafo grafoNuevo=crearGrafo(verticeConVecinos,cantidadVertices);
				
			return Algoritmo.conjuntoDominanteMinimo(grafoNuevo);
	}
	
	private static Grafo crearGrafo(HashMap<Integer,ArrayList<Integer>>verticeConVecinos,
			int cantidadVertices) {
		Grafo ret=generarAristas(verticeConVecinos,cantidadVertices);
				
		return ret;
	}
	
	private static Grafo generarAristas(HashMap<Integer,ArrayList<Integer>> verticesVecinosConPesos,
			int cantidadVertices) {		
		 grafo= new Grafo(cantidadVertices);		
		
		verticesVecinosConPesos.forEach(
				(clave,valor)->generarArista(clave,valor)
							
				);
					
		return grafo;
	}
	
	private static void generarArista(int verticeOriginal,ArrayList<Integer> verticesDestino) {

		for(Integer vertice:verticesDestino) {
			grafo.agregarArista(verticeOriginal, vertice);
		}
			
		
	}
	
	public static ArrayList<Integer> generarConjuntoMinimoRandom(HashMap<Integer,ArrayList<Integer>>verticeConVecinos,
			int cantidadVertices){
	
			Grafo grafoNuevo=crearGrafo(verticeConVecinos,cantidadVertices);
				
			
			return Algoritmo.conjuntoDominanteRandom(grafoNuevo);
	}
	

	
}
