package visual;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import logica.Provincia;
import logica.ProvinciaJSON;


@SuppressWarnings("serial")
public class MapaPanel extends JPanel {
	private JMapViewer mapa;
	private DatosConjuntoPanel datosPanel,datosPanelRandom;
	private ArrayList<Coordinate> coordenadas;
	private ArrayList<MapMarker> marcadores;
	private MapMarker markerActual;
	private HashMap<Coordinate,ArrayList<Coordinate>> verticesVecinos;
	private HashMap<Coordinate,Integer> verticesInteger;
	private HashMap<Integer,ArrayList<Integer>>verticeConVecinos;
	private boolean hayProvinciaSeleccionada;
	private int indexVertice;
	
	/**
	 * Create the panel.
	 */
	
	public MapaPanel() {
		this.setBounds(250, 100, 900, 900);
		this.coordenadas=new ArrayList<>(); 	
		this.datosPanel=new DatosConjuntoPanel();
		datosPanel.setSize(440, 225);
		datosPanel.setLocation(450, 11);
		this.datosPanel.setBounds(450, 149, 440, 180);
		
		
		this.marcadores=new ArrayList<>();
		this.verticesVecinos= new HashMap<>();
		this.verticeConVecinos=new HashMap<>();
		this.verticesInteger=new HashMap<>();
		this.indexVertice=0;
		
		this.mapa=new JMapViewer();
		this.hayProvinciaSeleccionada=false;
	
		this.mapa.setBounds(10, 55, 430, 834);
		Coordinate coordenada=new Coordinate(-38.416097, -63.616672);
		this.mapa.setDisplayPosition(coordenada, 5);
			
		this.mapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				agregarPunto((Coordinate)mapa.getPosition(e.getPoint()));
			}
		});
		
		setLayout(null);	
		this.add(datosPanel);
		this.add(mapa);
		
		datosPanelRandom = new DatosConjuntoPanel();
		datosPanelRandom.setBounds(450, 437, 440, 200);
		add(datosPanelRandom);
	}
					
	private void agregarPunto(Coordinate punto) {	
		ArrayList<Provincia>provincias=obtenerProvinciasJSON();
		
		Coordinate coordenada=new Coordinate(0,0);
		String provincia="";				
	
		for(int i=0;i<provincias.size();i++) {
			Provincia provinciaActual=provincias.get(i);
		
			if((esProvinciaValida(punto, provinciaActual) && this.verticesVecinos.isEmpty())) {
				
				coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
				coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
				
				this.markerActual=new MapMarkerDot(provincia,coordenada);
			
				markerActual.getStyle().setColor(Color.red);
				markerActual.getStyle().setBackColor(Color.red);
								
				this.marcadores.add(markerActual);
				this.coordenadas.add(coordenada);			
				this.hayProvinciaSeleccionada=true;
				this.verticesVecinos.put(markerActual.getCoordinate(), null);
				
				asociarCoordenadaInteger(markerActual.getCoordinate());
				mapa.addMapMarker(markerActual);
			}
			else if(esProvinciaValida(punto, provinciaActual)) {
				
						if(this.hayProvinciaSeleccionada) {							
																					
							coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
							coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
													
							MapMarker markerNew= new MapMarkerDot(provincia,coordenada);
							markerNew.getStyle().setColor(Color.red);
							
							
							if(!contieneVecino(markerActual,markerNew) && !verticesIguales(markerActual,markerNew)){
								
								this.hayProvinciaSeleccionada=false;								
								if(!this.coordenadas.contains(markerNew.getCoordinate())) {
									asociarCoordenadaInteger(markerNew.getCoordinate());
									this.marcadores.add(markerNew);
								}
																
								this.coordenadas.add(coordenada);
													
								agregarVecino(markerActual,markerNew);
								agregarVecino(markerNew,markerActual);
																
								mapa.addMapMarker(markerNew);
								
								this.markerActual.getStyle().setBackColor(Color.yellow);
						
								dibujarArista(markerNew,markerActual);
							}
																									
						}else {
							coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
							coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
							this.markerActual=new MapMarkerDot(provincia,coordenada);
													
							if(this.coordenadas.contains(markerActual.getCoordinate())) {		
								
								markerActual.getStyle().setColor(Color.red);
								markerActual.getStyle().setBackColor(Color.red);
																
								mapa.addMapMarker(markerActual);
																
								this.hayProvinciaSeleccionada=true;
							}else {
								lanzarAdvertencia("Debe seleccionar un vertice ya existente.");
							}
						}
			}
		}	
		actualizarMarcadores();				
	}

	private ArrayList<Provincia> obtenerProvinciasJSON() {
		ProvinciaJSON provinciaJSON=new ProvinciaJSON();
		return provinciaJSON.leerJSON("Provincias.JSON").getProvincias();
	}

	private boolean esProvinciaValida(Coordinate punto, Provincia provinciaActual) {
		return punto.getLat()<=provinciaActual.getLimiteLatArriba()
				&& punto.getLat()>=provinciaActual.getLimiteLatAbajo() 
				&& punto.getLon()>=provinciaActual.getLimiteLonIzquierda()
				&& punto.getLon()<=provinciaActual.getLimiteLonDerecha();
	}
	
	private void asociarCoordenadaInteger(Coordinate coordenada) {
		if(!contieneCoordenadaAsociada(coordenada)) {	
			
			this.verticesInteger.put(coordenada,this.indexVertice);		
			this.indexVertice++;
		}
	}
	
	private boolean contieneCoordenadaAsociada(Coordinate coordenada) {	
		this.verticesInteger.forEach((clave,valor)->sonCoordenadasIguales(clave,coordenada)
				);
		
		return false;
	}

	private boolean sonCoordenadasIguales(Coordinate clave, Coordinate coordenada) {
		return clave.equals(coordenada);
	}

	private boolean verticesIguales(MapMarker vertice, MapMarker verticeAComparar) {
		
		return vertice.getCoordinate().equals(verticeAComparar.getCoordinate());
	}
	
	private boolean contieneVecino(MapMarker vertice,MapMarker verticeAComparar) {
		if(this.verticesVecinos.get(vertice.getCoordinate())==null) {
			return false;
		}
		
		return this.verticesVecinos.get(vertice.getCoordinate()).contains(verticeAComparar.getCoordinate());
	}
	
	private void agregarVecino( MapMarker vertice,MapMarker verticeVecino) {
		ArrayList<Coordinate> vecinos=this.verticesVecinos.get(vertice.getCoordinate());
		
		if(vecinos==null) { 
			vecinos=new ArrayList<Coordinate>();
			vecinos.add(verticeVecino.getCoordinate());
			this.verticesVecinos.put(vertice.getCoordinate(), vecinos);		
		}else {
			vecinos.add(verticeVecino.getCoordinate());			
			this.verticesVecinos.put(vertice.getCoordinate(),vecinos);			
		}
		
		agregarVecinosIntegers(vertice, verticeVecino);
	}

	private void agregarVecinosIntegers(MapMarker vertice, MapMarker verticeVecino) {
		ArrayList<Integer> vecinosInteger=this.verticeConVecinos.get(this.verticesInteger.get(vertice.getCoordinate()));
		if(vecinosInteger==null) { 
			vecinosInteger=new ArrayList<Integer>();
			vecinosInteger.add(this.verticesInteger.get(verticeVecino.getCoordinate()));
			this.verticeConVecinos.put(this.verticesInteger.get(vertice.getCoordinate()),vecinosInteger );
			
		}else {			
			vecinosInteger.add(this.verticesInteger.get(verticeVecino.getCoordinate()));			
			this.verticeConVecinos.put(this.verticesInteger.get(vertice.getCoordinate()),vecinosInteger );			
		}
		
	}
	
	private void dibujarArista(MapMarker markerNew,MapMarker markerActual) {
		MapPolygonImpl polygon=new MapPolygonImpl(this.markerActual.getCoordinate(),this.markerActual.getCoordinate(),markerNew.getCoordinate());
		
		polygon.setColor(Color.blue);
		polygon.setBackColor(null);
				
		this.mapa.addMapPolygon(polygon);
	}
		
	private void lanzarAdvertencia(String mensaje) {
		JOptionPane.showMessageDialog(this,mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);
	}
	
	private void actualizarMarcadores() {
		this.mapa.removeAllMapMarkers();
		
		for(int i=0;i<this.marcadores.size();i++) {
			
			if(verticesIguales(this.marcadores.get(i),this.markerActual) && this.hayProvinciaSeleccionada) {
				this.marcadores.get(i).getStyle().setBackColor(Color.red);
			}else {				
				this.marcadores.get(i).getStyle().setBackColor(Color.yellow);
			}
			
			this.mapa.addMapMarker(this.marcadores.get(i));
		}
	}
	
	public void marcarVertices(ArrayList<Integer> vertices,DatosConjuntoPanel datosMapa) {		
		for(Integer vertice:vertices) {
			marcarVerticeDibujado(vertice,datosMapa);
			
		}
		
	}
	
	private void marcarVerticeDibujado(Integer vertice,DatosConjuntoPanel datosMapa) {
	
		verticesInteger.forEach(
				(clave,valor)->{
					if(valor==vertice) {
						datosMapa.escribirConjunto(nombreVertices(clave));
						marcar(clave);
					}
				}
						);
	}
	
	private String nombreVertices(Coordinate vertice) {	
		ArrayList<Provincia>provincias=obtenerProvinciasJSON();
				
		for(Provincia provincia:provincias) {
			if(provincia.getCoordenadaPrincipalLat()==vertice.getLat()) {
				return provincia.getNombre();
			}
		}
		return "";
	}
	
	private void marcar(Coordinate verticeDibujado) {		
		MapMarker markerNuevo1= new MapMarkerDot("",verticeDibujado);
			
		markerNuevo1.getStyle().setBackColor(Color.cyan);
		markerNuevo1.getStyle().setColor(Color.darkGray);
		
		this.mapa.addMapMarker(markerNuevo1);
		
	}
	
	public DatosConjuntoPanel getDatosPanel() {
		return datosPanel;
	}
	
	public DatosConjuntoPanel getDatosPanelRandom() {
		return datosPanelRandom;
	}
	
	public void limpiar() {
		this.mapa.removeAllMapMarkers();
		this.mapa.removeAllMapPolygons();
		this.coordenadas.clear();
		this.markerActual=null;
		this.marcadores.clear();
		this.hayProvinciaSeleccionada=false;
		this.verticesInteger.clear();
		this.verticesVecinos.clear();
		this.indexVertice=0;
		this.verticeConVecinos.clear();
		this.datosPanel.limpiar();
		this.datosPanelRandom.limpiar();
	}
		
	public ArrayList<MapMarker> getMarcadores() {
		return marcadores;
	}

	public HashMap<Integer, ArrayList<Integer>> getVerticeConVecinos() {
		return verticeConVecinos;
	}		
}