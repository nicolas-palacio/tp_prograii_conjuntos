package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import controlador.Controlador;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Cursor;



public class Visual {
	private JFrame frame;
	private MapaPanel mapaPanel;
	private JButton btnAlgoritmoRandom;
	private JLabel lblAristas;
	private JButton btnAlgoritmo;	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visual window = new Visual();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Visual() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setBounds(100, 100, 950, 950);
		this.frame.setTitle("Creador de Grafos");
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setResizable(false);

		this.mapaPanel= new MapaPanel();
		mapaPanel.getDatosPanel().setBounds(450, 47, 440, 328);
		
		    btnAlgoritmo = new JButton("Generar Conjunto Dominante");
		    btnAlgoritmo.setBounds(104, 218, 249, 57);
		    mapaPanel.getDatosPanel().add(btnAlgoritmo);
		    btnAlgoritmo.setFocusPainted(false);
		    btnAlgoritmo.setBackground(new Color(255, 255, 255));
		    btnAlgoritmo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		    
		    btnAlgoritmo.addMouseListener(new MouseAdapter() {
		    	@Override
		    	public void mouseClicked(MouseEvent e) {		
		    		ejecutarAlgoritmo();
		    	}
		    });
		    
		    btnAlgoritmo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.mapaPanel.setLayout(null);	
		frame.getContentPane().add(mapaPanel);
		
		this.lblAristas = new JLabel("Conjunto Dominante Minimo");
		this.lblAristas.setFont(new Font("Tahoma", Font.ITALIC, 20));
		this.lblAristas.setBounds(479, 11, 330, 40);
		this.mapaPanel.add(lblAristas);
			
			this.btnAlgoritmoRandom = new JButton("Generar conjunto random");
			btnAlgoritmoRandom.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					ejecutarAlgoritmoRandom();
				}
			});
			btnAlgoritmoRandom.setEnabled(true);
			btnAlgoritmoRandom.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			btnAlgoritmoRandom.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAlgoritmoRandom.setBackground(Color.WHITE);
			btnAlgoritmoRandom.setBounds(560, 663, 235, 57);
			mapaPanel.add(btnAlgoritmoRandom);
			
			JButton btnLimpiar = new JButton("Limpiar");
			btnLimpiar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					limpiarMapa();
				}
			});
			btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnLimpiar.setBackground(Color.WHITE);
			btnLimpiar.setBounds(833, 850, 87, 46);
			mapaPanel.add(btnLimpiar);
			
			JLabel lblAristas_1 = new JLabel("Conjunto Dominante Minimo Random");
			lblAristas_1.setFont(new Font("Tahoma", Font.ITALIC, 22));
			lblAristas_1.setBounds(485, 407, 415, 31);
			mapaPanel.add(lblAristas_1);
	}
			
	private void ejecutarAlgoritmo() {
		
		if(puedeEjecutar()) {
			int cantidadVertices=this.mapaPanel.getMarcadores().size();
			ArrayList<Integer> conjunto=Controlador.generarConjuntoMinimo(this.mapaPanel.getVerticeConVecinos(),cantidadVertices);
			
			this.mapaPanel.marcarVertices(conjunto,mapaPanel.getDatosPanel());
			this.btnAlgoritmo.setEnabled(false);
		}else {
			lanzarAdvertencia("El mapa no tiene vertices o posee solo uno.");
		}		
	}
	
	private boolean puedeEjecutar() {
		return this.mapaPanel.getMarcadores().size()>1;
	}
							
	private void lanzarAdvertencia(String mensaje) {
		JOptionPane.showMessageDialog(this.frame,
		 mensaje,
	    "Advertencia",
	    JOptionPane.WARNING_MESSAGE);
	}
	
	private void ejecutarAlgoritmoRandom() {
		if(puedeEjecutar()) {
			int cantidadVertices=this.mapaPanel.getMarcadores().size();
			ArrayList<Integer> conjunto=Controlador.generarConjuntoMinimoRandom(this.mapaPanel.getVerticeConVecinos(),cantidadVertices);	
			this.mapaPanel.marcarVertices(conjunto,mapaPanel.getDatosPanelRandom());
			this.btnAlgoritmoRandom.setEnabled(false);
			
		}else {
			lanzarAdvertencia("El mapa no tiene vertices o posee solo uno.");
		}
	}
	
	private void limpiarMapa() {
		this.mapaPanel.limpiar();	
		this.btnAlgoritmo.setEnabled(true);
		this.btnAlgoritmoRandom.setEnabled(true);
	}
}	