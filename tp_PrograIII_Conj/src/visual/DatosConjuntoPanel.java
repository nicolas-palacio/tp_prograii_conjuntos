package visual;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import javax.swing.border.CompoundBorder;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class DatosConjuntoPanel extends JPanel {
	private JTextArea textArea;
	private JScrollPane scrollTxtArea;
	
	/**
	 * Create the panel.
	 */
	public DatosConjuntoPanel() {
		setBorder(new CompoundBorder());
		setLayout(null);
		
		this.textArea = new JTextArea();	
		this.textArea.setEditable(false);
			
		this.scrollTxtArea=new JScrollPane(textArea);
		this.scrollTxtArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.scrollTxtArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.scrollTxtArea.setBounds(29, 11, 388, 150);
		
		add(this.scrollTxtArea);
			
	}
	
	public void escribirConjunto(String nombre) {	
		this.textArea.setText(this.textArea.getText()+nombre+"\n");	
	}
	
	public void limpiar() {
		this.textArea.setText("");
	}
}