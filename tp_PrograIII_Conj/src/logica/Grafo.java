package logica;

import java.util.ArrayList;
import java.util.HashMap;

public class Grafo {
	//Matriz de adyacencia
	private boolean[][] A; 
	private HashMap<Integer,ArrayList<Integer>> vecinos;
	private int vertices;
	
	public Grafo(int vertices) {
		this.A=new boolean[vertices][vertices];
		this.vertices=vertices;
		this.vecinos=new HashMap<Integer,ArrayList<Integer>>(); 
	}
			

	public boolean[][] getA() {
		return A;
	}
	public void agregarVertice(int i) {
		
		
		A[i][i]=true;
					
	}

	public void agregarArista(int i, int j) {
		verificarVertices(i,j);	
		
		A[i][j]=true;
		A[j][i]=true;				
	}
	
	public void borrarArista(int i,int j) {
		verificarVertices(i,j);
	
		A[i][j]=false;
		A[j][i]=false;
	}
		
	public boolean existeArista(int i,int j) {
		verificarVertices(i,j);
		
		return A[i][j];
	}
	
	private void verificarVertices(int i,int j) {
		if(i<0 || j<0) {
			throw new IllegalArgumentException("El vertice no puede ser negativo");
		}
		if(i>=this.cantidadVertices() || j>=this.cantidadVertices()) {
			throw new IllegalArgumentException("El vertice no puede ser mayor que la cantidad de vertices del grafo");
		}
		if(i==j) {
			throw new IllegalArgumentException("Los vertices no pueden ser iguales: "+i+"="+j);
		}
	}
	
		
	public int cantidadVertices() {
		return this.vertices;
	}	
		
	public HashMap<Integer, ArrayList<Integer>> getVecinos() {
		if(this.vecinos.isEmpty()) {
			obtenerTodosLosVecinos();
		}
		return this.vecinos;
	}



	private void obtenerTodosLosVecinos() {				
		for(int fila=0;fila<this.vertices;fila++) {
			ArrayList<Integer> vecinosVertice=new ArrayList<>();
			
			for(int columna=0;columna<this.getA()[0].length;columna++) {
				if(this.getA()[fila][columna]) {
					vecinosVertice.add(columna);
				}				
			}
			this.vecinos.put(fila, vecinosVertice);
					
		}

	}

	@Override
	public  String toString() {
		String ret="";
		for(int i=0;i<this.A.length;i++) { //fila
			ret+="estoy en vertice numero: "+i+"\n";
			for(int j=0;j<this.A[0].length;j++) {//columna
				ret+="arista: "+i+"--"+j;
			}
			ret+="\n";
		}
		return ret;
	}
		
}

